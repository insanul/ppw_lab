from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'', include('lab_1.urls')),
    # re_path(r'^lab-2/', include('lab_2.urls')),
    # re_path(r'^$', index_lab1, name='index')
]
