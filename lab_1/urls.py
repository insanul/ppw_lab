from django.urls import re_path, path
from .views import index, contact, message
#url for app
app_name='lab_1'
urlpatterns = [
    re_path(r'^$', index, name='index'),
    path('message/', message, name='message'),
    path('contact/', contact, name='contact'),
]
