from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
# Create your views here.
def index(request):
    response = {}
    return render(request, 'index_lab1.html', response)

def contact(request):
    response = {}
    return render(request, 'contact.html', response)

def message(request):
    response = {}
    return render(request, 'message.html', response)
